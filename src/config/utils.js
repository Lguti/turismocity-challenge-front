import axios from 'axios';

const BASE_URL = 'https://api.turismocity.com';
const PRODUCTS_ENDPOINT = '/cuarentena/products';
const axiosInstance = axios.create({ baseURL: BASE_URL });

export const getProducts = async () => {
    try {
        const res = await axiosInstance.get(PRODUCTS_ENDPOINT);
        return res.data;
    } catch (err) {
        console.error(err);
        throw err
    }
}